#!/bin/sh

#set -e

MULTIARCH=$(dpkg-architecture -qDEB_HOST_MULTIARCH)

cd obj-$MULTIARCH/tests && make
/usr/bin/ctest --force-new-ctest-process --rerun-failed --output-on-failure

